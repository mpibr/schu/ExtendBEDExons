#!/usr/bin/perl

# ExtendBedExons
# split BED12 file into features
#
# Input
#   -bed12  : BED12 annotation file
#   -genome : TAB delimited 2 columns file, describing
#             the genome of interest, 1st column is chromosome
#             name and 2nd column is chromosome size
#   -tpext  : integer, giving size to extended window after known 3' gene end
#   -fpext  : integer, giving size to extended window before known 5' gene start
#   -gff    : output in GFF file format
#   -refFlat: output in refFlat file format
#   -bed6   : output in BED6 file format
#   -bedFlat: output in BED6 file format but annotating fp_extended, fp_utr, cds, intron, tp_utr, tp_extended
#
# Output
#   filename_features_date.bed : BED12 features map with extended 5' and 3' exon
#
# Version 6.0
# Date Sep 2016
# Author go9kata

use warnings;
use strict;
use Getopt::Long();
use File::Basename;

sub usage($);
sub ParseGenomeFile($);
sub ParseBedFile($);
sub GetGeneGaps($$);
sub PrintExtendedData($$$$$$$);
sub CalculateShifts($$$$$);

MAIN:
{
    # define input selection
    my $bed_file;
    my $genome_file;
    my $tp_ext = 0;
    my $fp_ext = 0;
    my $gff_format;
    my $rfl_format;
    my $bds_format;
    my $help;
    
    # Set-up parameters
    Getopt::Long::GetOptions(
        "bed12=s" =>\$bed_file,
        "genome=s" =>\$genome_file,
        "fpext=i" =>\$fp_ext,
        "tpext=i" =>\$tp_ext,
        "gff" =>\$gff_format,
        "rfl" =>\$rfl_format,
        "bed6" =>\$bds_format,
        "help" => \$help
    ) or usage("Error::invalid command line options");
    
    # define help output
    usage("version 4.0, Feb 2016") if($help);
    
    # define input file
    usage("Error::query BED12 file name must be specified")
    unless defined $bed_file;
    my ($bed_name, $bed_path, $bed_suffix) = fileparse($bed_file, qr/\.[^.]*/);
    print STDERR "File name: $bed_name\n";
    
    usage("Error::reference TAB genome file name must be specified")
    unless defined $genome_file;
    
    # define extension span
    $fp_ext = 0 if($fp_ext < 0);
    $tp_ext = 0 if($tp_ext < 0);
    
    # check for output format
    my $format_condition = defined($gff_format) + defined($rfl_format) + defined($bds_format);
    
    if ($format_condition > 1)
    {
        usage("Error:choose only one output format either GFF, RFL, BED6 or default BED12");
    }
    
    # calculate ratio between extensions
    my $ratio_ext = (($fp_ext + $tp_ext) > 0) ? ($fp_ext / ($fp_ext + $tp_ext)) : 0;
    
    # read genome hash
    my ($genome_hash, $genome_lines) = ParseGenomeFile($genome_file);
    print STDERR "number of genome lines: $genome_lines\n";
    
    # read bed file
    my ($bed_hash, $bed_lines) = ParseBedFile($bed_file);
    print STDERR "number of bed12 lines: $bed_lines\n";
    
    # assign previous and next distance
    GetGeneGaps($bed_hash,$genome_hash);
    
    # print out results
    PrintExtendedData($bed_hash, $fp_ext, $tp_ext, $ratio_ext, $gff_format, $rfl_format, $bds_format);
}

# calculate shifts
sub CalculateShifts($$$$$)
{
    my $dist_prev = shift;
    my $dist_next = shift;
    my $fp_ext = shift;
    my $tp_ext = shift;
    my $ratio_ext = shift;
    
    my $shift_prev = 0;
    my $shift_next = 0;
    
    # calculates previous shift
    if (($dist_prev - $fp_ext - $tp_ext) > 0)
    {
        $shift_prev = $fp_ext;
    }
    else
    {
        $shift_prev = int($dist_prev * $ratio_ext);
    }
    
    # calculate next shift
    if (($dist_next - $fp_ext - $tp_ext) > 0)
    {
        $shift_next = $tp_ext;
    }
    else
    {
        $shift_next = int($dist_next * (1 - $ratio_ext));
    }
    
    return($shift_prev, $shift_next);
}


# print results
sub PrintExtendedData($$$$$$$)
{
    my $bed_hash = shift;
    my $fp_ext = shift;
    my $tp_ext = shift;
    my $ratio_ext = shift;
    my $gff_format = shift;
    my $rfl_format = shift;
    my $bds_format = shift;
    
    my $output_count = 0;
    
    # loop through the hash
    foreach my $key (sort keys %{$bed_hash})
    {
        # current array size
        my $ary_siz = scalar(@{$bed_hash->{$key}});
        
        # loop over each record
        for (my $k = 0; $k < $ary_siz; $k++)
        {
            # update output count
            $output_count++;
            
            # BED 12 records
            my $chrom = $bed_hash->{$key}[$k][0];
            my $chrom_start = $bed_hash->{$key}[$k][1];
            my $name = $bed_hash->{$key}[$k][3];
            my $score = $bed_hash->{$key}[$k][4];
            my $strand = $bed_hash->{$key}[$k][5];
            my $thick_start = $bed_hash->{$key}[$k][6];
            my $thick_end = $bed_hash->{$key}[$k][7];
            my $rgb_color = $bed_hash->{$key}[$k][8];
            my $blocks = $bed_hash->{$key}[$k][9];
            my @blockSizes = split(",", $bed_hash->{$key}[$k][10]);
            my @blockStarts = split(",", $bed_hash->{$key}[$k][11]);
            my $dist_prev = $bed_hash->{$key}[$k][12];
            my $dist_next = $bed_hash->{$key}[$k][13];
            
            # split name attributes
            my $transcript_id = sprintf("gid%06d", $output_count);
            my $gene_id = $name;
            if ($name =~ m/;/)
            {
                my @nameAttributes = split(";", $name);
                $transcript_id = $nameAttributes[0];
                $gene_id = $nameAttributes[1];
            }
            
            # calculate shifts based on strand
            my $shift_prev = 0;
            my $shift_next = 0;
            ($shift_prev, $shift_next) = CalculateShifts($dist_prev, $dist_next, $fp_ext, $tp_ext, $ratio_ext) if($strand eq "+");
            ($shift_prev, $shift_next) = CalculateShifts($dist_prev, $dist_next, $tp_ext, $fp_ext, $ratio_ext) if($strand eq "-");
            
            # update coordinates
            $blockStarts[0] -= $shift_prev;
            $blockSizes[0] += $shift_prev;
            $blockSizes[-1] += $shift_next;
            
            # loop over each exon
            my @exonsArray = ();
            my $blockSizesList = "";
            my $blockStartsList = "";
            my $exonLeftList = "";
            my $exonRightList = "";
            for (my $ex = 0; $ex < $blocks; $ex++)
            {
                my $exon_left = $chrom_start + $blockStarts[$ex];
                my $exon_right = $exon_left + $blockSizes[$ex];
                my $exon_id = ($strand eq "+") ? ($ex + 1) : ($blocks - $ex);
                
                # update exons array
                push(@exonsArray, [$exon_left, $exon_right, $exon_id]);
                
                # update exons information
                $exonLeftList .= $exon_left;
                $exonLeftList .= "," if($ex <($blocks-1));
                $exonRightList .= $exon_right;
                $exonRightList .= "," if($ex <($blocks-1));
                
                # update blocks information
                $blockSizesList .= ($exon_right - $exon_left);
                $blockSizesList .= "," if($ex < ($blocks-1));
                $blockStartsList .= $exon_left - $exonsArray[0][0];
                $blockStartsList .= "," if($ex < ($blocks-1));
                
            }
            
            # output formats
            if (defined($gff_format))
            {
                my $attributes = sprintf("gene_id %s; transcript_id %s", $gene_id, $transcript_id);
                for (my $ex = 0; $ex < $blocks; $ex++)
                {
                    print $chrom,"\t","Custom","\t","exon","\t",$exonsArray[$ex][0],"\t",$exonsArray[$ex][1],"\t",$exonsArray[$ex][2],"\t",$strand,"\t",".","\t",$attributes,"\n";
                }
            }
            elsif (defined($bds_format))
            {
                for (my $ex = 0; $ex < $blocks; $ex++)
                {
                    print $chrom,"\t",$exonsArray[$ex][0],"\t",$exonsArray[$ex][1],"\t",$name,"\t",$exonsArray[$ex][2],"\t",$strand,"\n";
                }
            }
            elsif (defined($rfl_format))
            {
                print $gene_id,"\t",$transcript_id,"\t",$chrom,"\t",$strand,"\t",$exonsArray[0][0],"\t",$exonsArray[-1][1],"\t",$thick_start,"\t",$thick_end,"\t",$blocks,"\t",$exonLeftList,"\t",$exonRightList,"\n";
            }
            else
            {
                print $chrom,"\t",$exonsArray[0][0],"\t",$exonsArray[-1][1],"\t",$name,"\t",$score,"\t",$strand,"\t",$thick_start,"\t",$thick_end,"\t",$rgb_color,"\t",$blocks,"\t",$blockSizesList,"\t",$blockStartsList,"\n";
            }
            
        }
    }
    
    return;
}


# calculate gene gaps
sub GetGeneGaps($$)
{
    my $bed_hash = shift;
    my $genome_hash = shift;
    
    foreach my $key (sort keys %{$bed_hash})
    {
        # sort current chromosome
        my $ary_ref = $bed_hash->{$key};
        my @ary_srt = sort {$a->[1] <=> $b->[1]} @{$ary_ref};
        my $ary_siz = scalar(@ary_srt);
        
        # current chromosome size
        my ($chrom, $strand) = split(";", $key, 2);
        my $chrom_siz = $genome_hash->{$chrom};
        
        for (my $k = 0; $k < $ary_siz; $k++)
        {
            # get previous record
            my $idx_prev = $k;
            my $stop_cond = 1;
            my $dist_prev = 0;
            while ($stop_cond)
            {
                # update backwards counter
                $idx_prev--;
                
                # update backwards coordinate
                my $pos_right_prev = ($idx_prev < 0) ? 0 : $ary_srt[$idx_prev][2];
                
                # update previous distance
                $dist_prev = $ary_srt[$k][1] - $pos_right_prev;
                
                # update stop condition
                $stop_cond = ($pos_right_prev <= $ary_srt[$k][1]) ? 0 : 1;
            }
            
            
            # get next record
            my $idx_next = $k;
            $stop_cond = 1;
            my $dist_next = 0;
            while ($stop_cond)
            {
                # update forward counter
                $idx_next++;
                
                # update forward coordinate
                my $pos_left_next = ($idx_next >= $ary_siz) ? $chrom_siz : $ary_srt[$idx_next][1];
                
                # update next distance
                $dist_next = $pos_left_next - $ary_srt[$k][2];
                
                # update stop condition
                $stop_cond = ($pos_left_next >= $ary_srt[$k][2]) ? 0 : 1;
            }
            
            # update array in hash
            push(@{$ary_srt[$k]},$dist_prev, $dist_next);
            
        }
        
        # update curren array
        $bed_hash->{$key} = \@ary_srt;
    }
    
    return;
}




# parse bed12 file
sub ParseBedFile($)
{
    my $query_file = shift;
    my %bed_hash = ();
    my $bed_lines = 0;
    
    # read file line by line
    open (my $fh, '<', $query_file) or die("Can't open $query_file to read: $!\n");
    while (<$fh>)
    {
        # skip header lines
        next if(($_ =~ m/^#/) || ($_ =~ m/^\n$/));
            
        # remove new line character
        chomp($_);
        
        # increment lines
        $bed_lines++;
        
        # split BED12 line
        my @bed_line = split("\t", $_, 12);
        
        # create key = chromosome + strand
        my $primary_key = $bed_line[0] . ";" . $bed_line[5];
        
        # fill up hash
        push(@{$bed_hash{$primary_key}}, \@bed_line);
        
    }
    close($fh);
    
    return (\%bed_hash, $bed_lines);
}

# parse genome file
sub ParseGenomeFile($)
{
    my $query_file = shift;
    my %genome_hash = ();
    my $genome_lines = 0;
    
    # read file line by line
    open (my $fh, '<', $query_file) or die("Can't open $query_file to read: $!\n");
    while (<$fh>)
    {
        # skip header lines
        next if(($_ =~ m/^#/) || ($_ =~ m/^\n$/));
            
        # remove new line character
        chomp($_);
        
        # increment lines
        $genome_lines++;
        
        # split record in columns
        my ($chrom_id, $chrom_length) = split("\t", $_, 2);
        
        # fill hash
        $genome_hash{$chrom_id} = $chrom_length;
        
    }
    close($fh);
    
    # return reference to hash
    return(\%genome_hash, $genome_lines);
}

sub usage($)
{
    my $message = $_[0];
    if (defined $message && length $message)
    {
        $message .= "\n" unless ($message =~ /\n$/);
    }
    
    my $command = $0;
    $command =~ s#^.*/##;
    
    print STDERR (
    
    $message,
    "usage: $command -bed bed_file.bed -genome genomeX.tab -fpext N -tpext N\n" .
    "description: Bed12ToBedExons split BED12 file into features\n" .
    "parameters: \n" .
    "-bed12\n" .
    "\tbed_file.bed :: query BED12 map from UCSC genome annotation\n" .
    "-genome\n" .
    "\tgenome_versionX.tab :: TAB delimited file describing genome content\n" .
    "\t\t1st column is chromosome name\n" .
    "\t\t2nd column is chromosome size\n" .
    "-fpext N\n" .
    "\tinteger, giving size to extended window before known 5' gene start\n" .
    "-tpext N\n" .
    "\tinteger, giving size to extended window after known 3' gene end\n" .
    "-gff\n" .
    "\toutput will be in GFF format\n" .
    "-rfl\n" .
    "\toutput will be in refFlat format\n" .
    "-bed6\n" .
    "\toutput will be in BED6 format\n" .
    "-help\n" .
    "\tdefine usage\n"
    );
    
    die("\n");
}